﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace website.Models
{
    public class Estado
    {
        [Key]
        public int EstadoID { get; set; }

        [Required(ErrorMessage = "Preencha o nome do Estado")]
        [DisplayName("Descrição")]
        [StringLength(50, MinimumLength = 3, ErrorMessage ="A descrição do estado deve ter entre 3 e 50 caracteres")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Preencha a sigla do estado")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "A sigla do estado deve conter 2 caracteres")]
        public string Sigla { get; set; }
    }
}