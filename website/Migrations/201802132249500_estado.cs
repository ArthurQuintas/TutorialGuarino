namespace website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class estado : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Estado",
                c => new
                    {
                        EstadoID = c.Int(nullable: false, identity: true),
                        Descricao = c.String(unicode: false),
                        Sigla = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.EstadoID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Estado");
        }
    }
}
