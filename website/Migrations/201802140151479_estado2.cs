namespace website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class estado2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Estado", "Descricao", c => c.String(nullable: false, maxLength: 50, storeType: "nvarchar"));
            AlterColumn("dbo.Estado", "Sigla", c => c.String(nullable: false, maxLength: 2, storeType: "nvarchar"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Estado", "Sigla", c => c.String(unicode: false));
            AlterColumn("dbo.Estado", "Descricao", c => c.String(unicode: false));
        }
    }
}
