namespace website.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using website.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<website.Models.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(website.Models.Context context)
        {
            IList<Estado> estados = new List<Estado>();
            estados.Add(new Estado { Descricao = "Esp�rito Santo", Sigla = "ES" });
            estados.Add(new Estado { Descricao = "Minas Gerais", Sigla = "MG" });
            estados.Add(new Estado { Descricao = "Paran�", Sigla = "PR" });
            estados.Add(new Estado { Descricao = "Santa Catarina", Sigla = "SC" });
            estados.Add(new Estado { Descricao = "S�o Paulo", Sigla = "SP" });
            estados.Add(new Estado { Descricao = "Rio de Janeiro", Sigla = "RJ" });
            estados.Add(new Estado { Descricao = "Rio Grande do Sul", Sigla = "RS" });

            foreach(Estado estado in estados)
            {
                context.Estados.AddOrUpdate(x => x.EstadoID, estado);
            }
        }
    }
}
